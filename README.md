# CW Ideas

## Events List/Calendar Module
> `cw_events_list_calendar`

- events listing and filtering by a calendar. similar to [Columbia Sports Events](www.columbiascsports.com/events).
- a better experience with JSON Exporting Data, and fed into a Vue or Angular App.
- would have `install configs` so it can be installed on sites that do not have `event` content type.
- include templates and basic styles.
